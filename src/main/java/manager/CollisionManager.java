
package manager;

import items.ItemShape;
import java.util.LinkedList;
import javafx.geometry.Bounds;
import javafx.scene.shape.Rectangle;
import snake.game.Block;
import snake.game.SPart;
import snake.game.Snake;

public class CollisionManager {
    public static int snakeColSnake(Snake snake){
        Rectangle head = snake.getParts().get(0).getBounds();
        for (int i = 1; i < snake.getParts().size(); i++) {
            Rectangle part = snake.getParts().get(i).getBounds();
            if(collisionRect(head, part)){
                snake.setStatus(Snake.COL_SNAKE);
                return Snake.COL_SNAKE;
            }
        }
        return Snake.COL_NONE;
    }
    
    public static int snakeColBlock(Snake snake, LinkedList<Block> blocks){
        Rectangle head = snake.getParts().get(0).getBounds();
        for (Block block : blocks) {
            if(block.isSolid() && collisionRect(head, block.getBounds())){
                snake.setStatus(Snake.COL_BLOCK);
                return Snake.COL_BLOCK;
            }
        }
        return Snake.COL_NONE;
    }
    
    public static int snakeColItem(Snake snake, LinkedList<ItemShape> items){
        Rectangle head = snake.getParts().get(0).getBounds();
        for (ItemShape item : items) {
            if(collisionRect(head, item.getBounds())){
                snake.setStatus(Snake.COL_ITEM);
                item.getItem().make(snake);
                items.remove(item);
                return Snake.COL_ITEM;
            }
        }
        return Snake.COL_NONE;
    }
    
    public static boolean collisionRect(Rectangle r1, Rectangle r2){
        if(r1.getX() < r2.getX() + r2.getWidth() &&
           r1.getX() + r1.getWidth() > r2.getX() && 
           r1.getY() < r2.getY() + r2.getHeight() &&
           r1.getY() + r1.getHeight() > r2.getY())
            return true;
        return false;
    }
    
    public static boolean collisionRectWithBlockAndSnacke(Rectangle rect, LinkedList<Block> blocks, LinkedList<ItemShape> items, Snake snake){
        
        for (int i = 0; i < snake.getParts().size(); i++) {
            Rectangle part = snake.getParts().get(i).getBounds();
            if(collisionRect(rect, part)){
                return true;
            }
        }
        for (Block block : blocks) {
            if(block.isSolid() && collisionRect(rect, block.getBounds())){
                return true;
            }
        }
        for (ItemShape item : items) {  //checked!
            if(collisionRect(rect, item.getBounds())){
                return true;
            }
        }
        
        return false;
    }
}
