
package manager;

import java.util.HashMap;
import java.util.LinkedList;
import javafx.scene.image.Image;

public class ImageManager {
    private HashMap<String, Image> images;
    private static ImageManager instance;
    private LinkedList<String> names;
    
    private ImageManager(){
        this.initName();
        this.initImages();
    }
    
    private void initName(){
        this.names = new LinkedList<>();
        this.names.add("wall");
        this.names.add("ground");
        this.names.add("head");
        this.names.add("body");
        this.names.add("tail");
        this.names.add("apple");
        this.names.add("curve");
        
    }
    
    private void initImages(){
        this.images = new HashMap<>();
        for (String name : this.names) {
            try{
                this.images.put(name, new Image("file:" + name + ".png"));
            }catch(Exception e){
                System.err.print("-> Image '" + name + "' not found!");
            }
        }
    }
    
    public static ImageManager getInstance(){
        if(instance == null)
            instance = new ImageManager();
        return instance;
    }
    
    public Image getImageByName(String name){
        return this.images.get(name);
    }
}
