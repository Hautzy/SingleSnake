
package popupfx;

import java.io.IOException;
import javafx.application.Application;
import static javafx.application.ConditionalFeature.FXML;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PopUpMain extends Application {

    @FXML
    private Label scoreLabel;
    @FXML
    private Label messageLabel;

    @Override
    public void start(Stage primaryStage) throws IOException {

        AnchorPane root = FXMLLoader.load(PopUpMain.class.getResource("PopUpFX.fxml"));

        for (Node n : root.getChildren()) {
            try {
                Label label = (Label) n;
                if (label.getId().equals("scoreLabel")) {
                    scoreLabel = label;
                } else {
                    messageLabel = label;
                }
            } catch (ClassCastException ex) {

            }
        }

        Scene scene = new Scene(root, 800, 600);
        primaryStage.setTitle("Game Over");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public void showPopup(int score, String message) {
        scoreLabel.setText(Integer.toString(score));
        messageLabel.setText(message);
    }

    @FXML
    private void exitBtnClicked() {
        Stage window = (Stage) scoreLabel.getScene().getWindow();
        if (window != null) {
            window.close();
        }
    }

}
