
package items;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import manager.ImageManager;

public class ItemShape {
    public static final int NORMAL_ITEM_WIDTH = 16;
    public static final int NORMAL_ITEM_HEIGHT = 16;
    public static final boolean DRAW_COLLISION_BOXES = false;
    public static final boolean PRINT_ITEM_INFO = false;

    private Point2D position;
    private Rectangle bounds;
    private int width;
    private int height;
    private int rotation;
    
    private String name;
    
    private Item item;
    
    public ItemShape(String name, int x, int y, int width, int height) {
        this.name = name;

        this.width = NORMAL_ITEM_WIDTH;
        this.height = NORMAL_ITEM_HEIGHT;
        this.position = new Point2D(x * this.width, y * this.height);

        this.rotation = 0;
        this.bounds = new Rectangle(this.position.getX(), this.position.getY(), this.width, this.height);
        
        this.item = ItemManager.getInstance().getItemByName(name);
    }

    public Item getItem() {
        return item;
    }

    private void setItem(Item item) {
        this.item = item;
    }
    
    public String getName() {
        return name;
    }

    public Point2D getPosition() {
        return position;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getRotation() {
        return rotation;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void render(GraphicsContext gc) {
        gc.drawImage(ImageManager.getInstance().getImageByName(name), position.getX(), position.getY(), width, height);
        if (DRAW_COLLISION_BOXES) {
            gc.save();
            gc.setStroke(Color.RED);
            gc.strokeRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
            gc.restore();
        }
        if (PRINT_ITEM_INFO) {
            System.out.println(toString());
        }
    }

    @Override
    public String toString() {
        return String.format("N=%s|X=%f|Y=%f|W=%d|H=%d", name, position.getX()/16, position.getY()/16, width, height);
    }
    
}
