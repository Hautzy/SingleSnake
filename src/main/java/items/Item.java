
package items;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import manager.ImageManager;
import static snake.game.Block.DRAW_COLLISION_BOXES;
import static snake.game.Block.PRINT_BLOCK_INFO;
import snake.game.Snake;
import snake.game.Snake;

public abstract class Item {

    

    private String name;

    public Item(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public abstract void make(Snake snake);
}
