
package items;

import java.util.HashMap;
import java.util.Random;
import javafx.scene.shape.Rectangle;
import manager.CollisionManager;
import snake.game.SPartType;
import snake.game.Snake;
import snake.game.World;

public class ItemManager {
    private HashMap<String, Item> registeredItems;    
    
    private static ItemManager instance;
    
    private ItemManager(){
        this.registeredItems = new HashMap<>();
        this.registerItems();
    }

    public static ItemManager getInstance(){
        if(instance == null)
            instance = new ItemManager();
        return instance;
    }
    
    private void registerItems() {
        this.registeredItems.put("apple", new Item("apple") {
            
            @Override
            public void make(Snake snake) {
                snake.setScore(snake.getScore() + 10);
                snake.getParts().getLast().setType(SPartType.BODY);
                snake.getParts().addLast(snake.getLastPart());
                System.out.println("-Snake ate an apple!");
            }
        });
    }
    
    public Item getItemByName(String name){
        return this.registeredItems.get(name);
    }
    
    public ItemShape getNewItem(World world){
        Random rand = new Random();
        int index = 0;
        int minX = 0;
        int minY = 0;
        int maxX = world.getWidth() - 1;
        int maxY = world.getHeight() - 1;
        Rectangle rect = null;
        int rX = 0;
        int rY = 0;
        
        do{
            rX = rand.nextInt(maxX - minX) + minX;
            rY = rand.nextInt(maxY - minY) + minY;
        
            rect = new Rectangle(rX*ItemShape.NORMAL_ITEM_WIDTH, rY*ItemShape.NORMAL_ITEM_HEIGHT, ItemShape.NORMAL_ITEM_WIDTH, ItemShape.NORMAL_ITEM_HEIGHT);
        
        }while(CollisionManager.collisionRectWithBlockAndSnacke(rect, world.getLoadedBlocks(), world.getLoadedItems(), world.getSnake()));
        
        ItemShape itemShape = new ItemShape("apple" , rX, rY, ItemShape.NORMAL_ITEM_WIDTH, ItemShape.NORMAL_ITEM_HEIGHT);
        //Collision Detection
        return itemShape;
    }
}
