
package snake.game;

import items.Item;
import items.ItemManager;
import items.ItemShape;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import manager.CollisionManager;
import popupfx.PopUpMain;

public class World {

    private LinkedList<Block> loadedBlocks;
    private LinkedList<ItemShape> loadedItems;
    private Image image;

    private Snake snake;
    private Canvas canvas;
    private GameLoop gameLoop;

    public World(Canvas canvas, GameLoop gameLoop) {
        this.gameLoop = gameLoop;
        this.loadedBlocks = new LinkedList<>();
        this.loadedItems = new LinkedList<>();
        this.snake = new Snake(10, 3, 8);
        this.canvas = canvas;

        this.loadWorld();

        this.loadedItems.add(ItemManager.getInstance().getNewItem(this));
        //this.loadedItems.add(ItemManager.getInstance().getNewItem(this));
        //this.loadedItems.add(ItemManager.getInstance().getNewItem(this));
        //this.loadedItems.add(ItemManager.getInstance().getNewItem(this));
        //this.loadedItems.add(ItemManager.getInstance().getNewItem(this));
    }

    public void loadWorld() {
        System.out.println("...load map...");
        this.image = new Image("file:map.png");
        PixelReader pr = image.getPixelReader();
        for (int y = 0; y < this.image.getHeight(); y++) {
            for (int x = 0; x < this.image.getWidth(); x++) {
                Color color = pr.getColor(x, y);
                if (color.equals(Color.BLACK)) {
                    this.loadedBlocks.add(new Block("wall", true, x, y, Block.NORMAL_BLOCK_WIDTH, Block.NORMAL_BLOCK_HEIGHT));
                } else if (color.equals(Color.WHITE)) {
                    this.loadedBlocks.add(new Block("ground", false, x, y, Block.NORMAL_BLOCK_WIDTH, Block.NORMAL_BLOCK_HEIGHT));
                }
            }
        }
        this.image = null;
    }

    public void render(GraphicsContext gc) {
        for (Block loadedBlock : loadedBlocks) {
            loadedBlock.render(gc);
        }
        for (ItemShape loadedItem : loadedItems) {
            loadedItem.render(gc);
        }
        this.snake.render(gc);
        //gc.strokeText("Score: " + this.snake.getScore(), x, y);
        //System.out.println("Score: " + this.snake.getScore());
        if (this.snake.getStatus() == Snake.COL_BLOCK) {
            gameLoop.stop();
            createPopUp(this.snake.getScore(), "You pumped into a wall!");
        } else if (this.snake.getStatus() == Snake.COL_SNAKE) {
            gameLoop.stop();
            createPopUp(this.snake.getScore(), "Watch out for your tail!");
        }
    }

    private void createPopUp(int score, String message) {
        Label scoreLabel = null;
        Label messageLabel = null;
        Stage primaryStage = new Stage();
        
        AnchorPane root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/PopUpFX.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(PopUpMain.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Node n : root.getChildren()) {
            if(n instanceof Label){
                Label label = (Label)n;
                if (label.getId().equals("scoreLabel")) {
                    scoreLabel = label;
                }else {
                    messageLabel = label;
                }
            }else if(n instanceof Button){
                Button button = (Button)n;
                button.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event) {
                        Platform.exit();
                    }
                });
            }
        }
        
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.initOwner(canvas.getScene().getWindow());
        Scene scene = new Scene(root, 800, 600);
        primaryStage.setTitle("Game Over");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        scoreLabel.setText("Punktestand: " + Integer.toString(score));
        messageLabel.setText(message);
    }

    public int getWidth() {
        return (int) this.canvas.getWidth() / 16;
    }

    public int getHeight() {
        return (int) this.canvas.getHeight() / 16;
    }

    public Block getByRelativePosition(int x, int y) {
        for (Block block : this.loadedBlocks) {
            if ((int) (block.getPosition().getX() / block.getWidth()) == x && (int) (block.getPosition().getY() / block.getHeight()) == y) {
                return block;
            }
        }
        return null;
    }

    public Block getByRealPosition(int x, int y) {
        for (Block block : this.loadedBlocks) {
            if ((int) block.getPosition().getX() == x && (int) block.getPosition().getY() == y) {
                return block;
            }
        }
        return null;
    }

    public void tick(long startMilliTime) {
        if (this.snake.getStatus() != Snake.COL_NONE && this.snake.getStatus() != Snake.COL_ITEM) {
            return;
        }

        this.snake.tick(startMilliTime);

        CollisionManager.snakeColSnake(snake);
        CollisionManager.snakeColBlock(snake, loadedBlocks);
        CollisionManager.snakeColItem(snake, loadedItems);

        //System.out.println(snake.getStatus());
        //snake.setStatus(0);
        if (this.snake.getStatus() == Snake.COL_ITEM) {
            this.loadedItems.add(ItemManager.getInstance().getNewItem(this));
            snake.setStatus(Snake.COL_NONE);
        }
    }

    public LinkedList<Block> getLoadedBlocks() {
        return loadedBlocks;
    }

    public Snake getSnake() {
        return snake;
    }

    public LinkedList<ItemShape> getLoadedItems() {
        return loadedItems;
    }

}
