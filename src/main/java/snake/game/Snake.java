
package snake.game;

import java.util.LinkedList;

import javafx.scene.canvas.GraphicsContext;
import manager.KeyManager;

public class Snake {
    
    public static final int COL_NONE = 0;
    public static final int COL_SNAKE = 1;
    public static final int COL_BLOCK = 2;
    public static final int COL_ITEM = 3;
    
    private LinkedList<SPart> parts;
    
    private Direction curDir;
    
    private int velocity;
    
    private int status;
    
    private int score;
    
    private SPart lastPart;
    
    public Snake(int x, int y, int length){
        this.status = COL_NONE;
        this.curDir = Direction.RIGHT;
        this.velocity = 2;
        this.parts = new LinkedList<>();
        this.score = 0;
        this.lastPart = null;
        //use length!
        for (int i = 0; i < length; i++) {
            if(i == 0)
                this.parts.add(new SPart((x-i)*SPart.NORMAL_SNAKE_WIDTH, y*SPart.NORMAL_SNAKE_HEIGHT, SPartType.HEAD, Direction.RIGHT));
            else if(i == length - 1)
                this.parts.add(new SPart((x-i)*SPart.NORMAL_SNAKE_WIDTH, y*SPart.NORMAL_SNAKE_HEIGHT, SPartType.TAIL, Direction.RIGHT));
            else
                this.parts.add(new SPart((x-i)*SPart.NORMAL_SNAKE_WIDTH, y*SPart.NORMAL_SNAKE_HEIGHT, SPartType.BODY, Direction.RIGHT));
        }
    }

    public SPart getLastPart() {
        return lastPart;
    }
    
    public void render(GraphicsContext gc) {    //right render
        for (int i = 0; i < this.parts.size(); i++) {
            SPart cur = this.parts.get(i);
            cur.setRotation(Direction.valueOf(cur.getDirection().toString()).ordinal());
            if(cur.getType() != SPartType.HEAD && cur.getType() != SPartType.TAIL){
                Direction next = this.parts.get(i - 1).getDirection();
                Direction prev = this.parts.get(i + 1).getDirection();
                Direction cDir = cur.getDirection();
                if(prev != next && cDir == prev){ //normal curve
                    cur.setCurved(true);
                    if(prev == Direction.UP){
                        if(next == Direction.LEFT){
                            cur.setRotation(2); //2*90 grad -> rotation
                        }else{
                            cur.setRotation(1); //1*90 grad -> rotation
                        }
                    }else if(prev == Direction.RIGHT){
                        if(next == Direction.UP){
                            cur.setRotation(3); //3*90 grad -> rotation
                        }else{
                            cur.setRotation(2); //2*90 grad -> rotation
                        }
                    }else if(prev == Direction.DOWN){
                        if(next == Direction.LEFT){
                            cur.setRotation(3); //3*90 grad -> rotation
                        }else{
                            cur.setRotation(0); //0*90 grad -> rotation
                        }
                    }else{
                        if(next == Direction.DOWN){
                            cur.setRotation(1); //1*90 grad -> rotation
                        }else{
                            cur.setRotation(0); //0*90 grad -> rotation
                        }
                    }
                }else if(prev != next && prev != cDir && next != cDir){ //U-curve
                    cur.setCurved(true);
                    if(prev == Direction.UP && next == Direction.DOWN){
                        if(cDir == Direction.RIGHT)
                            cur.setRotation(2);
                        else
                            cur.setRotation(1);
                    }else if(prev == Direction.RIGHT && next == Direction.LEFT){
                        if(cDir == Direction.UP)
                            cur.setRotation(2);
                        else
                            cur.setRotation(3);
                    }else if(prev == Direction.DOWN && next == Direction.UP){
                        if(cDir == Direction.RIGHT)
                            cur.setRotation(3);
                        else
                            cur.setRotation(0);
                    }else{
                        if(cDir == Direction.UP)
                            cur.setRotation(1);
                        else
                            cur.setRotation(0);
                    }
                }else if(prev == next && cDir != prev){ //S-curve
                    cur.setCurved(true);
                    if(prev == Direction.UP){
                        if(cDir == Direction.RIGHT){
                            cur.setRotation(3);
                        }else{
                            cur.setRotation(0);
                        }
                    }else if(prev == Direction.RIGHT){
                        if(cDir == Direction.UP){
                            cur.setRotation(1);
                        }else{
                            cur.setRotation(0);
                        }
                    }else if(prev == Direction.DOWN){
                        if(cDir == Direction.RIGHT){
                            cur.setRotation(2);
                        }else{
                            cur.setRotation(1);
                        }
                    }else{
                        if(cDir == Direction.UP){
                            cur.setRotation(2);
                        }else{
                            cur.setRotation(3);
                        }
                    }
                }
                
            }
            if(cur.getType() == SPartType.TAIL){
                cur.setDirection(this.parts.get(i - 1).getDirection());
                cur.setRotation(Direction.valueOf(cur.getDirection().toString()).ordinal());
            }
            cur.render(gc);
        }
    }

    private void move(){
        getCurDirection();
        SPart head = moveHead();
        this.lastPart = this.parts.remove(this.parts.size() - 1);
        
        for (int i = 0; i < this.parts.size(); i++) {
            if(i == this.parts.size() - 1)
                this.parts.get(i).setType(SPartType.TAIL);
            else
                this.parts.get(i).setType(SPartType.BODY);
        }
        this.parts.add(0, head);
    }
    
    private void getCurDirection(){
        if(KeyManager.KEY_RIGHT){
            if(this.curDir != Direction.LEFT)
                this.curDir = Direction.RIGHT;
        }
        if(KeyManager.KEY_LEFT){
            if(this.curDir != Direction.RIGHT)
                this.curDir = Direction.LEFT;
        }
        if(KeyManager.KEY_UP){
            if(this.curDir != Direction.DOWN)
                this.curDir = Direction.UP;
        }
        if(KeyManager.KEY_DOWN){
            if(this.curDir != Direction.UP)
                this.curDir = Direction.DOWN;
        }
        
    }
    
    private SPart moveHead(Direction dir){
        SPart newHead = null;
        if(dir == Direction.RIGHT){
            newHead = new SPart((int)this.parts.get(0).getPosition().getX() + SPart.NORMAL_SNAKE_WIDTH, (int)this.parts.get(0).getPosition().getY(), SPartType.HEAD, curDir);
        }else if(dir == Direction.LEFT){
            newHead = new SPart((int)this.parts.get(0).getPosition().getX() - SPart.NORMAL_SNAKE_WIDTH, (int)this.parts.get(0).getPosition().getY(), SPartType.HEAD, curDir);
        }else if(dir == Direction.UP){
            newHead = new SPart((int)this.parts.get(0).getPosition().getX(), (int)this.parts.get(0).getPosition().getY() - SPart.NORMAL_SNAKE_HEIGHT, SPartType.HEAD, curDir);
        }else if(dir == Direction.DOWN){
            newHead = new SPart((int)this.parts.get(0).getPosition().getX(), (int)this.parts.get(0).getPosition().getY() + SPart.NORMAL_SNAKE_HEIGHT, SPartType.HEAD, curDir);
        }
        return newHead;
    }
    
    public void tick(long startMilliTime) {
        move();
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public LinkedList<SPart> getParts() {
        return parts;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
/*-----------------------------------TODO for students------------------------------------------------*/
    private SPart moveHead(){   //this method should be edited by the students of the course
        SPart head = null;
        
        //move to the right
        if(this.curDir == Direction.RIGHT){
            head = moveHead(Direction.RIGHT);
        }
        //move to the left
        if(this.curDir == Direction.LEFT){
            head = moveHead(Direction.LEFT);
        }
        //move up
        if(this.curDir == Direction.UP){
            head = moveHead(Direction.UP);
        }
        //move down
        if(this.curDir == Direction.DOWN){
            head = moveHead(Direction.DOWN);
        }
        
        return head;
    }
}
