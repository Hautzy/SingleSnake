
package snake.game;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import manager.KeyManager;

public class GameLoop extends AnimationTimer{
    public final static long DELAY_TICK = 3000 / 32;    //ticks - 32 * Tick/Update per second
    public final static long DELAY_RENDER = 1000 / 32;  //render-rate - 60 * render per second
    
    private long currentMilliTime;
    private long lastMilliTimeTick;
    private long lastMilliTimeRender;
    
    private GraphicsContext gc;
    
    private World world;

    public GameLoop(Canvas canvas){
            
        this.currentMilliTime = System.currentTimeMillis();
        this.lastMilliTimeTick = 0;
        this.lastMilliTimeRender = 0;
        
        this.gc = canvas.getGraphicsContext2D();
        
        this.world = new World(canvas, this);
        
    }


    @Override
    public void handle(long now) {
        if(KeyManager.KEY_P)    return;
        this.currentMilliTime = System.currentTimeMillis();
        if(this.currentMilliTime - this.lastMilliTimeTick >= DELAY_TICK){
            tick(currentMilliTime);
            this.lastMilliTimeTick = System.currentTimeMillis();
        }
        
        this.currentMilliTime = System.currentTimeMillis();
        if(this.currentMilliTime - this.lastMilliTimeRender >= DELAY_RENDER){
            gc.clearRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());  //clear canvas
            render(gc);
            this.lastMilliTimeRender = System.currentTimeMillis();
        }
    }

    private void render(GraphicsContext gc){
        this.world.render(gc);
        
    }

    private long tick(long startMilliTime){
        this.world.tick(startMilliTime);
        
        return System.currentTimeMillis() - startMilliTime;
    }
    
}
