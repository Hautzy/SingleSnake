
package snake.game;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import manager.ImageManager;

public class SPart {

    public static final int NORMAL_SNAKE_WIDTH = 16;
    public static final int NORMAL_SNAKE_HEIGHT = 16;

    private Point2D position;
    private int width;
    private int height;
    private SPartType type;
    private Direction direction;
    private boolean curved;

    private int rotation;

    public SPart(int x, int y, SPartType type, Direction direction) {
        this.width = NORMAL_SNAKE_WIDTH;
        this.height = NORMAL_SNAKE_HEIGHT;
        this.position = new Point2D(x, y);
        this.type = type;
        this.direction = direction;
        this.rotation = 0;
        this.curved = false;
    }

    public boolean isCurved() {
        return curved;
    }

    public void setCurved(boolean curved) {
        this.curved = curved;
    }
    
    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    public void setRealPosition(int x, int y) {
        this.position = new Point2D(x, y);
    }

    public void setType(SPartType type) {
        this.type = type;
    }

    public Point2D getPosition() {
        return position;
    }

    public SPartType getType() {
        return type;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public Rectangle getBounds() {
        return new Rectangle(this.position.getX(), this.position.getY(), width, height);
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void render(GraphicsContext gc) {
        Image img = null;
        if (curved) {
            img = ImageManager.getInstance().getImageByName("curve");
        } else {
            img = ImageManager.getInstance().getImageByName(this.type.toString().toLowerCase());
        }
        gc.save();
        Rotate r = new Rotate(90 * this.rotation, this.position.getX() + width / 2, this.position.getY() + height / 2);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
        gc.drawImage(img, this.position.getX(), this.position.getY(), width, height);
        gc.restore();
        curved = false;
    }
}
