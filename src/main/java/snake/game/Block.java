
package snake.game;

import manager.ImageManager;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.Rectangle;

public class Block {
    public static final int NORMAL_BLOCK_WIDTH = 16;
    public static final int NORMAL_BLOCK_HEIGHT = 16;
    public static final boolean DRAW_COLLISION_BOXES = false;
    public static final boolean PRINT_BLOCK_INFO = false;
    
    private Point2D position;
    private Rectangle bounds;
    private int width;
    private int height;
    private int rotation;
    
    private String name;
    
    private boolean solid;
    
    public Block(String name, boolean solid, int x, int y, int width, int height){
        this.name = name;
        
        this.solid = solid;
        
        this.width = width;
        this.height = height;
        this.position = new Point2D(x*this.width, y*this.height);
        
        this.rotation = 0;
        this.bounds = new Rectangle(position.getX(), position.getY(), this.width, this.height);
    }

    public String getName() {
        return name;
    }

    public Point2D getPosition() {
        return position;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getRotation() {
        return rotation;
    }

    /*public void setPosition(Point2D position) {
        this.position = position;
        this.bounds = new Rectangle(position.getX(), position.getY(), width, height);
    }*/

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    public boolean isSolid() {
        return solid;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    
    public void render(GraphicsContext gc) {
        gc.drawImage(ImageManager.getInstance().getImageByName(name), position.getX(), position.getY(), width, height);
        if(DRAW_COLLISION_BOXES)
            gc.strokeRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
        if(PRINT_BLOCK_INFO)
            System.out.println(toString());
    }

    @Override
    public String toString() {
        return String.format("N=%s|S=%b|X=%f|Y=%f|W=%d|H=%d", name, solid, position.getX(), position.getY(), width, height);
    }
    
}
