package snake;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import manager.KeyManager;

public class MainApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                KeyCode k = event.getCode();
                if (k == KeyCode.W) {
                    KeyManager.KEY_UP = true;
                }
                if (k == KeyCode.S) {
                    KeyManager.KEY_DOWN = true;
                }
                if (k == KeyCode.D) {
                    KeyManager.KEY_RIGHT = true;
                }
                if (k == KeyCode.A) {
                    KeyManager.KEY_LEFT = true;
                }
                if (k == KeyCode.P){
                    KeyManager.KEY_P = !KeyManager.KEY_P;
                    System.out.println(KeyManager.KEY_P);
                }
            }

        });
        scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                KeyCode k = event.getCode();
                //System.out.println(k.getName());
                if (k == KeyCode.W) {
                    KeyManager.KEY_UP = false;
                }
                if (k == KeyCode.S) {
                    KeyManager.KEY_DOWN = false;
                }
                if (k == KeyCode.D) {
                    KeyManager.KEY_RIGHT = false;
                }
                if (k == KeyCode.A) {
                    KeyManager.KEY_LEFT = false;
                }
                if (k == KeyCode.K) {
                    //KeyManager.KEY_P = false;
                }
            }

        });
        
        stage.setTitle("JavaFX and Maven");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
