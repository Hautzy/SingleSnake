package snake;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import snake.game.GameLoop;
import manager.ImageManager;
import snake.game.World;

public class FXMLController implements Initializable {
    
    @FXML
    private Canvas canvas;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        canvas.getGraphicsContext2D().setFont(Font.font("Verenda", 20));
        ImageManager imgLoader = ImageManager.getInstance();
        GameLoop gameLoop = new GameLoop(canvas);
        gameLoop.start();
    }   
    
}
